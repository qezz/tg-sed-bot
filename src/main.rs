use std::env;

use futures::future;

use dotenv::dotenv;

use carapax::{
    App, FnHandler,
    context::Context,
    core::{
        types::Update, 
        Api, UpdateMethod, UpdatesStream,
        Config
    },
};

mod handler;
use handler::replace_text_handler;

mod sender;
mod store;

#[cfg(test)]
mod smoke_tests {
    #[allow(unused_imports)]
    use super::*;

    #[test]
    fn smoke() {

    }
}

fn main() {
    color_backtrace::install();

    dotenv().ok();
    env_logger::init();

    let token = env::var("SEDBOT_TOKEN").expect("SEDBOT_TOKEN is not set");
    let proxy = env::var("SEDBOT_PROXY").ok();

    let mut config = Config::new(token);
    if let Some(proxy) = proxy {
        config = config.proxy(proxy);
    }

    let api = Api::new(config.clone()).unwrap();
    tokio::run(future::lazy(move || {
        let store = store::Store::empty();
        let message_sender = sender::MessageSender::new(api.clone(), store.clone());
        let setup_context = move |context: &mut Context, _update: Update| {
            context.set(store.clone());
            context.set(config.clone());
            context.set(message_sender.clone())
        };
        App::new()
            .add_handler(FnHandler::from(setup_context))
            .add_handler(FnHandler::from(replace_text_handler))
            .run(api.clone(), UpdateMethod::poll(UpdatesStream::new(api)))
    }))
}
