use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use carapax::core::types::{Integer};

use failure::Error;
use futures::{
    future, Future,
};

#[derive(Clone)]
pub struct Store {
    // Maps a 'command' message in some chat to a 'result' message in some chat
    hm: Arc<Mutex<HashMap<(Integer, Integer), (Integer, Integer)>>>
}

impl Store {
    pub fn empty() -> Store {
        Store {
            hm: Arc::new(Mutex::new(HashMap::new()))
        }
    }

    pub fn track_message(
        &self,
        chat_id: Integer,
        input_message_id: Integer,
        result_message_id: Integer,
    ) -> impl Future<Item = (), Error = Error> {

        self.hm.lock().unwrap().insert(
            (chat_id, input_message_id), (chat_id, result_message_id)
        );

        future::ok(())
    }

    pub fn get_tracked_message(
        &self,
        chat_id: Integer, 
        command_message_id: Integer
    ) -> impl Future<Item = Option<(Integer, Integer)>, Error = Error> {
        match self.hm.lock().unwrap().get(&(chat_id, command_message_id)) {
            Some(tuple) => future::ok(Some(tuple.clone())),
            None => {
                future::ok(None)
            },
        }
    }
}